/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

addEventListener('notificationclick', (event) => {
  event.waitUntil(
    (async function () {
      const allClients = await clients.matchAll({
        includeUncontrolled: true,
      });
      let chatClient;
      // Let's see if we already have a chat window open:
      for (const client of allClients) {
        const url = new URL(client.url);
        if (url.pathname == '/menu') {
          // Excellent, let's use it!
          client.focus();
          chatClient = client;
          break;
        }
      }
      // If we didn't find an existing chat window,
      // open a new one:
      if (!chatClient) {
        chatClient = await clients.openWindow('/menu');
      }
      // Message the client:
      // chatClient.postMessage('New chat messages!');
      event.notification.close();
    })()
  );
});

// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
var firebaseConfig = {
  apiKey: 'AIzaSyBgPHdHGuufWT-fiGlLOD_8zvjxG83Gc2M',
  authDomain: 'satgasmada.firebaseapp.com',
  projectId: 'satgasmada',
  storageBucket: 'satgasmada.appspot.com',
  messagingSenderId: '204696039953',
  appId: '1:204696039953:web:2956b7320f54c0eef237b0',
  measurementId: 'G-0XQPG2HPE2',
};

firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  const notif = payload.data;
  // Customize notification here
  const notificationTitle = notif.title;
  const notificationOptions = {
    body: notif.body,
    icon: '/img/icons/apple-touch-icon-120x120.png',
  };
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
