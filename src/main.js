import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import '@/assets/css/main.css';

Vue.config.productionTip = false;

import Convert from './plugins/convert';
import VeeValidate from './plugins/veeValidate';
import VueFirestore from 'vue-firestore';
import Vue2GoogleMaps from './plugins/Vue2GoogleMaps';

Vue.use(VueFirestore);

new Vue({
  router,
  store,
  Convert,
  Vue2GoogleMaps,

  VeeValidate,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
