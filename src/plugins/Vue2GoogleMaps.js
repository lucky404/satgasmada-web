import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyA1u6V5E13D9H2RHOpSjOCIhpYkUcxlkUk',
    libraries: 'places',
    region: 'ID',
    language: 'id',
  },
  installComponents: true,
});

export default VueGoogleMaps;
