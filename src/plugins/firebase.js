import { firebase } from '@firebase/app';
import '@firebase/firestore';
import '@firebase/messaging';
import '@firebase/auth';

var firebaseConfig = {
  apiKey: 'AIzaSyBgPHdHGuufWT-fiGlLOD_8zvjxG83Gc2M',
  authDomain: 'satgasmada.firebaseapp.com',
  projectId: 'satgasmada',
  storageBucket: 'satgasmada.appspot.com',
  messagingSenderId: '204696039953',
  appId: '1:204696039953:web:2956b7320f54c0eef237b0',
  measurementId: 'G-0XQPG2HPE2',
};

firebase.initializeApp(firebaseConfig);

export let messaging = null;

if (firebase.messaging.isSupported()) {
  messaging = firebase.messaging();

  messaging.usePublicVapidKey(
    'BD4QJOss-CfFmEL3_jgvJwB-Hzm_kuavlNCADUaBBnkSPqKdI9QG35yeEbfJgAMF-I4O37PQuyzOmRDFbU5Fp3w'
  );
}

export const db = firebase.firestore();

export default firebase;
