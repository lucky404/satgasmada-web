import Vue from 'vue';
import Vuex from 'vuex';

import firebase, { db } from '@/plugins/firebase';

import { userService } from '@/services/Storage.Service.js';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    status: '',
    user: userService.getUser() || null,
    profile: userService.getProfile() || null,
  },
  mutations: {
    auth_request(state) {
      state.status = 'loading';
    },
    auth_success(state, data) {
      state.status = 'success';
      if (data) {
        const user = {
          displayName: data.displayName,
          email: data.email,
          emailVerified: data.emailVerified,
          isAnonymous: data.isAnonymous,
          photoURL: data.photoURL,
          providerData: data.providerData,
          refreshToken: data.refreshToken,
          uid: data.uid,
        };
        state.user = user;
      } else {
        state.user = null;
      }
    },
    auth_error(state) {
      state.status = 'error';
    },
    logout(state) {
      state.status = '';
      state.user = '';
    },
    update_profile(state, data) {
      state.profile = data;
    },
  },
  getters: {
    isLoggedIn: (state) => !!state.user,
    profile: (state) => state.profile,
    authStatus: (state) => state.status,
  },
  actions: {
    async login({ dispatch, commit }, request) {
      commit('auth_request');
      try {
        const { email, password } = request;
        const { user } = await firebase
          .auth()
          .signInWithEmailAndPassword(email, password);
        commit('auth_success', user);
        userService.updateUser(user);
        db.collection('users').doc(user.uid).update({
          last_login: new Date(),
        });
        await dispatch('getProfile');
        return user;
      } catch (e) {
        commit('auth_error');
        throw e;
      }
    },
    async register({ dispatch, commit }, request) {
      commit('auth_request');
      try {
        const { email, password } = request;
        const { user } = await firebase
          .auth()
          .createUserWithEmailAndPassword(email, password);

        db.collection('users')
          .doc(user.uid)
          .set({
            ...request,
          });

        commit('auth_success', user);
        userService.updateUser(user);
        dispatch('getProfile');
        return user;
      } catch (e) {
        commit('auth_error');
        throw e;
      }
    },
    async getProfile({ commit }) {
      const user = firebase.auth().currentUser;
      if (user) {
        const profile = await db.collection('users').doc(user.uid).get();
        if (profile.exists) {
          const userData = { uid: profile.id, ...profile.data() };
          commit('update_profile', userData);
          userService.updateProfile(userData);
        }
      }
    },
    getLoggedInUser({ dispatch, commit }) {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          commit('auth_success', user);
          userService.updateUser(user);
          dispatch('getProfile');
        } else {
          dispatch('clearSession');
        }
      });
    },
    async logout({ commit }) {
      try {
        await firebase.auth().signOut();
        commit('logout');
        userService.removeSession();
      } catch (e) {
        commit('auth_error');
        throw e;
      }
    },
    clearSession({ commit }) {
      try {
        commit('logout');
        userService.removeSession();
      } catch (e) {
        commit('auth_error');
        throw e;
      }
    },
  },
});
