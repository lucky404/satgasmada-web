const Login = () =>
  import(/* webpackChunkName: "dashboard.login" */ '@/views/public/Login.vue');
const Register = () =>
  import(
    /* webpackChunkName: "dashboard.login" */ '@/views/public/Register.vue'
  );
const error404 = () =>
  import(
    /* webpackChunkName: "dashboard.errorPage404" */ '@/views/public/404.vue'
  );
const error500 = () =>
  import(
    /* webpackChunkName: "dashboard.errorPage500" */ '@/views/public/500.vue'
  );
const error401 = () =>
  import(
    /* webpackChunkName: "dashboard.errorPage401" */ '@/views/public/401.vue'
  );

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    onlyLoggedOut: true,
    meta: {
      title: 'Masuk',
    },
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    onlyLoggedOut: true,
    meta: {
      title: 'Daftar',
    },
  },
  {
    path: '/404',
    name: '404',
    component: error404,
    meta: {
      title: '404',
    },
  },
  {
    path: '/500',
    name: '500',
    component: error500,
    meta: {
      title: '500',
    },
  },
  {
    path: '/401',
    name: '401',
    component: error401,
    meta: {
      title: '401',
    },
  },
];
export default routes.map((route) => {
  const meta = {
    public: true,
    title: route.meta.title,
    onlyLoggedOut: route.onlyLoggedOut,
  };
  return { ...route, meta };
});
