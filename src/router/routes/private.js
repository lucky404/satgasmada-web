const Menu = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/Menu.vue'
  );
const AnnouncementIndex = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/announcement/Index.vue'
  );
const UserIndex = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/user/Index.vue'
  );
// const UserDetail = () =>
//   import(
//     /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/user/Detail.vue'
//   );
// const MeetingRoom = () =>
//   import(
//     /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/user/MeetingRoom.vue'
//   );
// const UserDetailLaporan = () =>
//   import(
//     /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/user/detail/Laporan.vue'
//   );
const MonitoringMap = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/monitoring/Map.vue'
  );
const ReportIndex = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/report/Index.vue'
  );
const ReportDetail = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/report/Detail.vue'
  );
// const ReportCategoryIndex = () =>
//   import(
//     /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/reportCategory/Index.vue'
//   );
const routes = [
  {
    path: '/menu',
    name: 'menu',
    component: Menu,
    meta: {
      title: 'Menu',
    },
  },
  {
    path: '/map',
    name: 'Map',
    component: MonitoringMap,
    meta: {
      title: 'Map',
    },
  },
  // {
  //   path: '/call/:uid',
  //   name: 'Call',
  //   component: MeetingRoom,
  //   meta: {
  //     title: 'Meeting',
  //   },
  //   roles: ['1', '2', '3'],
  // },
  {
    path: '/report',
    name: 'report',
    component: ReportIndex,
    meta: {
      title: 'Laporan',
    },
  },
  {
    path: '/report/detail/:id',
    name: 'report-detail',
    component: ReportDetail,
    meta: {
      title: 'Detail Laporan',
    },
  },
  {
    path: '/user',
    name: 'user',
    component: UserIndex,
    meta: {
      title: 'Master Data Pengguna',
    },
  },
  // {
  //   path: '/user/detail/:id',
  //   component: UserDetail,
  //   roles: ['1', '2', '3'],
  //   meta: {
  //     title: 'Detail Pengguna',
  //   },
  //   children: [
  //     {
  //       path: 'laporan',
  //       name: 'user-laporan',
  //       component: UserDetailLaporan,
  //       meta: {
  //         title: 'Laporan Pengguna',
  //       },
  //       roles: ['1', '2', '3'],
  //     },
  //   ],
  // },
  {
    path: '/announcement',
    name: 'announcement',
    component: AnnouncementIndex,
    meta: {
      title: 'Pengumuman',
    },
  },
];

export default routes.map((route) => {
  const meta = {
    public: false,
    title: route.meta.title,
  };
  return { ...route, meta };
});
