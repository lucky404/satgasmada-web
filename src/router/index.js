import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import routes from '@/router/routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  linkExactActiveClass: 'active',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: { name: '404' },
    },
    {
      path: '/',
      redirect: { name: 'menu' },
    },
  ].concat(routes),
});

router.beforeEach((to, from, next) => {
  const authenticated = store.getters.isLoggedIn;
  const isAdmin = store.getters.profile?.role === 'admin';
  const onlyLoggedOut = to.matched.some((record) => record.meta.onlyLoggedOut);
  const isPublic = to.matched.some((record) => record.meta.public);
  if (!isPublic && !authenticated) {
    return next({
      path: '/login',
    });
  }

  if (authenticated && onlyLoggedOut) {
    return next('/menu');
  }

  if (authenticated && !isPublic && !isAdmin) {
    return next('/401');
  }
  document.title = `Satgasmada Admin | ${to.meta.title}`;
  next();
});

export default router;
