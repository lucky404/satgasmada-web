import { db } from '@/plugins/firebase';
import errorHandler from '@/mixins/errorHandler';

const ref = db.collection('users');

export default {
  mixins: [errorHandler],
  data() {
    return {
      user: {},
      users: [],
      loadUsers: true,
      loadFormUser: false,
      loadUser: false,
      roles: [
        { label: 'Admin', value: 'admin' },
        { label: 'User', value: 'user' },
      ],
    };
  },
  methods: {
    async getUsers() {
      this.loadUsers = true;
      try {
        const resp = await ref.get();
        const data = [];
        resp.forEach((doc) => data.push({ id: doc.id, ...doc.data() }));
        this.users = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadUsers = false;
      }
    },
    async getUser(id) {
      this.loadUser = true;
      try {
        const resp = await ref.doc(id).get();
        if (resp.exists) {
          this.user = resp.data();
        } else {
          throw this.handleError('Data Not Found!');
        }
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadUser = false;
      }
    },
    async updateUser(request, id) {
      this.loadFormUser = true;
      const payload = {
        name: request.name,
        email: request.email,
        mobilephone: request.mobilephone,
        avatar: request.avatar,
        role: request.role,
        isActive: request.isActive,
      };
      try {
        await ref.doc(id).update(payload);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormUser = false;
      }
    },
  },
};
