import { db } from '@/plugins/firebase';
import errorHandler from '@/mixins/errorHandler';

const ref = db.collection('reports');

export default {
  mixins: [errorHandler],
  data() {
    return {
      report: {},
      reports: [],
      loadReports: true,
      loadReport: false,
      field: 'createdAt',
      page: 1,
      itemsPerPage: 8,
    };
  },
  methods: {
    async getReports(search = '', last) {
      this.loadReports = true;
      try {
        let resp = [];
        if (last) {
          resp = await ref
            .orderBy(this.field)
            .startAt(search)
            .endAt(search + '\uf8ff')
            .startAfter(last[this.field])
            .limit(this.itemsPerPage)
            .get();
        } else {
          resp = await ref.orderBy(this.field).limit(this.itemsPerPage).get();
        }

        const data = [];
        resp.docs.forEach(async (doc) => {
          const report = { id: doc.id, ...doc.data() };
          const user = await report.userId.get();
          report.user = user.data();
          data.push(report);
        });

        this.reports = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadReports = false;
      }
    },
    async getReport(id) {
      this.loadReport = true;
      try {
        const resp = await ref.doc(id).get();
        if (resp.exists) {
          this.report = resp.data();
          const user = await this.report.userId.get();
          this.report = { ...this.report, ...user.data() };
          this.report.createdAt = new Date(this.report.createdAt.toDate());
          return this.report;
        } else {
          throw this.handleError('Data Not Found!');
        }
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadReport = false;
      }
    },
  },
};
