export default {
  data() {
    return {
      error: {
        message: '',
      },
      formErrors: {},
    };
  },
  methods: {
    handleError(err) {
      this.error.message = err.toString();
      return { errors: { message: err.toString() } };
    },
    clearError() {
      this.error.message = '';
      this.formErrors = {};
    },
  },
};
