import { db } from '@/plugins/firebase';
import errorHandler from '@/mixins/errorHandler';
import fcm from '@/mixins/fcm';

const ref = db.collection('announcements');

export default {
  mixins: [errorHandler, fcm],
  data() {
    return {
      announcement: {},
      announcements: [],
      loadAnnouncements: true,
      loadFormAnnouncement: false,
      loadAnnouncement: false,
    };
  },
  methods: {
    async getAnnouncements() {
      this.loadAnnouncements = true;
      try {
        const resp = await ref.get();
        const data = [];
        resp.forEach((doc) => data.push({ id: doc.id, ...doc.data() }));
        this.announcements = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadAnnouncements = false;
      }
    },
    async getAnnouncement(id) {
      this.loadAnnouncement = true;
      try {
        const resp = await ref.doc(id).get();
        if (resp.exists) {
          this.announcement = resp.data();
        } else {
          throw this.handleError('Data Not Found!');
        }
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadAnnouncement = false;
      }
    },
    async createAnnouncement(request) {
      this.loadFormAnnouncement = true;
      const payload = {
        title: request.title,
        content: request.content,
        createdAt: new Date(),
      };
      try {
        const resp = await ref.add(payload);
        await this.pushGroupNotification(request.title, request.content);
        return resp;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormAnnouncement = false;
      }
    },
    async updateAnnouncement(request, id) {
      this.loadFormAnnouncement = true;
      const payload = {
        title: request.title,
        content: request.content,
      };
      try {
        await ref.doc(id).update(payload);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormAnnouncement = false;
      }
    },
    async deleteAnnouncement(id) {
      this.loadFormAnnouncement = true;
      try {
        await ref.doc(id).delete();
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormAnnouncement = false;
      }
    },
  },
};
