import errorHandler from '@/mixins/errorHandler';
export default {
  mixins: [errorHandler],
  data() {
    return {
      loadingAuth: false,
    };
  },
  methods: {
    async login(request) {
      this.loadingAuth = true;
      const data = {
        email: request.email,
        password: request.password,
      };
      try {
        const user = await this.$store.dispatch('login', data);
        return user;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadingAuth = false;
      }
    },
    async register(request) {
      this.loadingAuth = true;
      const data = {
        name: request.name,
        email: request.email,
        mobilephone: request.mobilephone,
        password: request.password,
        avatar: '',
        fcm_token: '',
        role: 'user',
        last_latitude: '',
        last_longitude: '',
        last_location_datetime: '',
        last_login: new Date(),
        isActive: false,
      };
      try {
        const user = await this.$store.dispatch('register', data);
        return user;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadingAuth = false;
      }
    },
    async getLoggedInUser() {
      this.loadingAuth = true;
      try {
        await this.$store.dispatch('getLoggedInUser');
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadingAuth = false;
      }
    },
  },
};
