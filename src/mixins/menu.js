export default {
  data() {
    return {
      menu: [],
    };
  },
  created() {
    this.menu = [
      {
        title: 'Monitoring',
        icon: require('@/assets/icons/monitoring-dark.svg'),
        url: 'Map',
      },
      {
        title: 'Laporan',
        icon: require('@/assets/icons/laporan-dark.svg'),
        url: 'report',
      },
      {
        title: 'Pemberitahuan',
        icon: require('@/assets/icons/pengumuman-dark.svg'),
        url: 'announcement',
      },
      {
        title: 'Anggota',
        icon: require('@/assets/icons/anggota-dark.svg'),
        url: 'user',
      },
      {
        title: 'Beranda',
        icon: require('@/assets/icons/home-dark.svg'),
        url: 'menu',
      },
    ];
  },
};
