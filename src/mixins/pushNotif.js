import firebase, { messaging, db } from '@/plugins/firebase';
import { mapGetters } from 'vuex';
import fcm from '@/mixins/fcm';
export default {
  mixins: [fcm],
  computed: mapGetters(['isLoggedIn', 'profile']),
  methods: {
    async getFirebaseMessagingToken() {
      if (firebase.messaging.isSupported()) {
        const permission = await navigator.permissions.query({
          name: 'notifications',
        });
        if (!this.isLoggedIn && permission.state === 'granted') {
          const oldToken = messaging.getToken();
          if (oldToken === localStorage.getItem('TOKEN_FCM')) {
            messaging.deleteToken(oldToken);
            localStorage.setItem('TOKEN_FCM', '');
          }
        } else if (this.isLoggedIn) {
          if (permission.state !== 'granted') {
            try {
              await messaging.requestPermission();
              window.location.reload();
            } catch (e) {
              // eslint-disable-next-line no-console
              console.warn('User Menolak akses notifikasi');
            }
          } else {
            try {
              const refreshedToken = await messaging.getToken();
              await this.subscribeTopic(refreshedToken);
              // eslint-disable-next-line no-console
              console.log(refreshedToken);
              this.updateTokenFirebase(refreshedToken);
            } catch (e) {
              // eslint-disable-next-line no-console
              console.error(e);
            }
          }
        }
      }
    },
    updateTokenFirebase(refreshedToken) {
      if (refreshedToken !== localStorage.getItem('TOKEN_FCM')) {
        db.collection('users').doc(this.profile.uid).update({
          fcm_token: refreshedToken,
        });
        localStorage.setItem('TOKEN_FCM', refreshedToken);
      }
    },
  },
};
