import errorHandler from '@/mixins/errorHandler';

export default {
  mixins: [errorHandler],
  data() {
    return {
      center: {
        lat: -6.5711,
        lng: 107.7616,
      },
      infoContent: '',
      infoWindowPos: {
        lat: -6.5711,
        lng: 107.7616,
      },
      zoom: 10,
      infoWinOpen: false,
      currentMidx: null,
      infoOptions: {
        pixelOffset: {
          width: 14,
          height: 0,
        },
      },
      markers: [],
      radius: 5000,
    };
  },
  methods: {
    insertMarker(lat, lng, content) {
      this.$gmapApiPromiseLazy().then(() => {
        this.center = {
          lat: parseFloat(lat),
          lng: parseFloat(lng),
        };
        this.markers.push({
          content,
          position: {
            lat: parseFloat(lat),
            lng: parseFloat(lng),
          },
          // icon: {
          //   url:
          //     'https://cdn3.iconfinder.com/data/icons/map-and-navigation-25/50/49-512.png',
          //   // eslint-disable-next-line no-undef
          //   scaledSize: new google.maps.Size(50, 50),
          //   // eslint-disable-next-line no-undef
          //   origin: new google.maps.Point(0, 0),
          //   // eslint-disable-next-line no-undef
          //   anchor: new google.maps.Point(25, 47)
          // }
        });
      });
    },
    resetMap() {
      this.markers = [];
      this.infoContent = '';
      this.infoWinOpen = false;
    },
    toggleInfoWindow(marker, idx) {
      this.infoWindowPos = marker.position;
      this.infoContent = this.getInfoWindowContent(marker);

      if (this.currentMidx == idx) {
        this.infoWinOpen = !this.infoWinOpen;
      } else {
        this.infoWinOpen = true;
        this.currentMidx = idx;
      }
      this.markers[idx].animation = this.markers[idx].animation == 1 ? 0 : 1;
      setTimeout(() => {
        this.markers[idx].animation = 0;
      }, 3000);
    },
    getInfoWindowContent(marker) {
      return `
          <div id="iw-container">
            <div class="iw-title">
              Title
            </div>
            <div class="iw-subTitle">
              ${marker.cotent}
            </div>
          </div>
        `;
    },
  },
};
