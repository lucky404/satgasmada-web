import axios from 'axios';
import errorHandler from '@/mixins/errorHandler';
const SERVER_KEY =
  'AAAAL6jVshE:APA91bHyrZtvrwfNW5XLNI3PrRVjeCCuOc1s7uCuzQg06Pc5P9alJ-_w_BeyPLQRf35pz4tvph0UGZBZqOElkIbDJuErC-pMlOsk4Rg7KIZrruSnH2UQ6z52Jes3XJGna7YPpeGsBVjc';
const TOPIC_NAME = 'SATGASMADANOTIF';
export default {
  mixins: [errorHandler],
  methods: {
    async subscribeTopic(fcmToken = '') {
      const token = fcmToken || localStorage.getItem('TOKEN_FCM');

      if (token) {
        try {
          await axios.post(
            `https://iid.googleapis.com/iid/v1/${token}/rel/topics/${TOPIC_NAME}`,
            {},
            {
              headers: {
                Authorization: `Bearer ${SERVER_KEY}`,
              },
            }
          );
        } catch (e) {
          // eslint-disable-next-line no-console
          console.error(e);
        }
      }
    },
    async pushGroupNotification(title, body) {
      const payload = {
        notification: {
          title,
          body,
          // click_action: 'http://localhost:3000/',
          // icon: 'http://url-to-an-icon/icon.png',
        },
        to: `/topics/${TOPIC_NAME}`,
      };
      try {
        await axios.post('https://fcm.googleapis.com/fcm/send', payload, {
          headers: {
            Authorization: `Bearer ${SERVER_KEY}`,
          },
        });
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
      }
    },
  },
};
