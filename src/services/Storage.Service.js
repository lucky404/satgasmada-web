const USER = 'access_user';
const PROFILE = 'access_profile';

/**
 * Manage the how Access Tokens are being stored and retreived from storage.
 *
 * Current implementation stores to localStorage. Local Storage should always be
 * accessed through this instace.
 **/
const userService = {
  getUser() {
    return JSON.parse(localStorage.getItem(USER));
  },
  updateUser(accessUser) {
    localStorage.setItem(USER, JSON.stringify(accessUser));
  },
  getProfile() {
    const profile = JSON.parse(localStorage.getItem(PROFILE));
    return profile;
  },
  updateProfile(accessProfile) {
    localStorage.setItem(PROFILE, JSON.stringify(accessProfile));
  },
  removeSession() {
    localStorage.removeItem(USER);
    localStorage.removeItem(PROFILE);
  },
};

export { userService };
