module.exports = {
  chainWebpack: (config) => {
    config.plugin('prefetch').tap((options) => {
      if (!options[0].fileBlacklist) options[0].fileBlacklist = [];
      options[0].fileBlacklist.push(/(.*)\.route\.(.*)\.js|css|css\.map$/);
      return options;
    });
  },
  transpileDependencies: ['vuetify'],
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  pwa: {
    workboxPluginMode: 'GenerateSW',
    name: 'Satgasmada Admin',
    background_color: '#11271c',
    themeColor: '#11271c',
    msTileColor: '#000000',
    appleMobileWebAppStatusBarStyle: 'black',
    gcm_sender_id: '646201884057',
    manifestOptions: {
      name: 'Satgasmada Admin',
      short_name: 'Satgasmada Admin',
      background_color: '#11271c',
      start_url: '.',
      display: 'standalone',
      theme_color: '#11271c',
    },
  },
};
